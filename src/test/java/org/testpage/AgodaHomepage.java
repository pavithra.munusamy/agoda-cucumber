package org.testpage;

import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import net.bytebuddy.asm.Advice.AllArguments;

public class AgodaHomepage {
	
	WebDriver driver;
	public AgodaHomepage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	@FindAll({
		@FindBy(xpath = "//button[@aria-label='Close Message']"),
		@FindBy(xpath = "//button[@aria-label='Close Message']")
	})
	private WebElement closeadd;
	
	public void closecoupon() throws InterruptedException {
		closeadd.click();
	}
	
	@FindBy(xpath = "//h6[text()='Flight + Hotel']")
	private WebElement flighthotel;
	
	public void clickFlight() {
		flighthotel.click();
	}
	
	@FindBy(xpath = "//button[@data-element-name='flight-option-button']")
	private WebElement roundoneway;
	
	public void click_roundandoneway() {
		roundoneway.click();
	}

	@FindBy(xpath = "//span[text()='One-way']")
	private WebElement oneway;
	
	public void Click_oneway(){
		oneway.click();
	}
	
	@FindBy(xpath="//span[text()='Round-trip']")
	private WebElement RoundTrip;
	
	public void click_Round() {
		RoundTrip.click();
	}
	
	@FindBy(xpath = "//button[@data-element-name='flight-cabin-class-button']")
	private WebElement economybutton;
	
	public void click_economybtn() {
		economybutton.click();
	}
	
	@FindBy(xpath="//span[text()='Economy']")
	private WebElement economy;
		
		public void click_Economy() {
			economy.click();
	}
	
		@FindBy(xpath="//span[text()='Premium economy']")
		private WebElement premium;
		
		public void click_Premium() {
			premium.click();
		}
		
		@FindBy(xpath = "//span[text()='Business']")
		private WebElement business;
		
		public void click_Business() {
			business.click();
		}
		
		@FindBy(xpath = "//span[text()='First']")
		private WebElement first;
		
		public void click_First() {
			first.click();
		}
		
		@FindBy(xpath = "(//span[text()='City or airport name'])[1]")
		private WebElement from;
		
		public void click_from() {
			from.click();
		}
		
		@FindBy(xpath = "(//span[text()='City or airport name'])[2]")
		private WebElement to;
		
		public void click_To() {
			to.click();
		}
		
		@FindBy(xpath = "//span[text()='New Delhi and NCR, India']")
		private WebElement NewDelhi;
		
		public void select_newDelhi() {
			NewDelhi.click();
		}
		
		@FindBy(xpath = "//span[text()='Mumbai, India']")
		private WebElement mumbai;
		
		public void select_Mumbai() {
			mumbai.click();
		}
		
		@FindBy(xpath = "//span[text()='Bangalore, India']")
		private WebElement banglore;
		
		public void select_Banglore() {
			banglore.click();
		}
		
		@FindBy(xpath = "//span[text()='Departure']")
		private WebElement departure;
		
		public void click_Departure() {
			departure.click();
		}
		
		@FindBy(xpath = "(//span[text()='1'])[1]")
		private WebElement passeddates;
		
		public void click_Departuredatespassed() {
			passeddates.click();
		}
		
		@FindBy(xpath = "(//span[text()='30'])[1]")
		private WebElement previousdate;
		
		public void click_Departuredatesprevious() {
			previousdate.click();
		}
		
		@FindBy(xpath = "(//span[text()='3'])[1]")
		private WebElement currentdate;
		
		public void click_Departuredatescurrent() {
			currentdate.click();
		}
		
		@FindBy(xpath = "(//span[text()='12'])[1]")
		private WebElement afterdates;
		
		public void click_Departuredatesafter() {
			afterdates.click();
		}
		
		@FindBy(xpath = "//div[text()='1 Passenger']")
		private WebElement passenger;
		
		public void click_Passenger() {
			passenger.click();				
		}
		
		
		@FindAll({
		@FindBy(xpath = "(//button[@aria-label='+'])[1]"),
		@FindBy(xpath="(//span[@data-selenium='plus'])[1]"),
		@FindBy(xpath = "(//span[@data-element-name='flight-occupancy-adult-increase'])[1]")
		})
		private WebElement adult;
		
		public void click_Adult() throws InterruptedException {
			
			for(int i=0;i<=2;i++ ) {
				adult.click();
		}
		}
		
		@FindBy(xpath = "(//span[@data-selenium='plus'])[2]")
		private WebElement children;
		
		public void click_Children() {
			for(int i=0;i<=3;i++ ) {
				children.click();
		}	
		}
		
		@FindBy(xpath = "(//span[@data-selenium='plus'])[2]")
		private WebElement infants;
		
		public void click_Infants() {
			for(int i=0;i<=3;i++ ) {
				infants.click();
		}	
		}
		
		@FindBy(xpath = "//span[text()='Continue']")
		
		private WebElement contiune;
		
		public void click_Contiune() {
			contiune.click();
		}
		
		@FindBy(xpath = "(//div[@class='SearchBoxTextDescription__title'])[3]")
		private WebElement roomoccupancy;
		
		public void roomoccupency() {
			roomoccupancy.click();
		}
		
		@FindBy(xpath = "//div[@class='Popup__content']//div[@data-selenium='room-option-2']//span[@class='Spanstyled__SpanStyled-sc-16tp9kb-0 gwICfd kite-js-Span ']")
		private WebElement room;
		
		public void click_Room() {
			room.click();
		/*	Actions a=new Actions(driver);
			a.keyDown(Keys.CONTROL);
			a.click(); 
			Select s=new Select(room);
			s.selectByVisibleText("2"); */
		}
		
		@FindBy(xpath="//span[text()='Return']")
		private WebElement Return;
		
		public void click_return() {
			Return.click();
		}
		
		@FindBy(xpath = "(//span[text()='1'])[1]")
		private WebElement returncurrentdate;
		
		public void click_returncurrdate() {
			returncurrentdate.click();
		}
		
		@FindBy(xpath="(//span[text()='2'])[1]")
		
		private WebElement returncurrentafterdate;
		
		public void click_returncurrafterdate() {
			returncurrentafterdate.click();
		}
		
		@FindBy(xpath="(//span[text()='1'])[2]")
		
		private WebElement returnnextmonth;
		
		public void click_returnnextmonth() {
			returnnextmonth.click();
		}
		
		@FindBy(xpath = "//span[text()='Search for hotel in different cities or dates']")
		
		private WebElement tickcheckbox;
		public void click_Checkbox() {
			tickcheckbox.click();
		}
		
		@FindBy(xpath = "//span[text()='SEARCH FLIGHT + HOTEL']")
		
		private WebElement clicksearch;
		public void click_search() {
			clicksearch.click();
		}
		
		@FindBy(xpath = "(//button[@type='button'])[3]")
		
		private WebElement clickcross;
		
		public void click_Cross() {
			clickcross.click();
		}
		
		@FindBy(xpath = "//span[text()='Check-in']")
		private WebElement checkin;
		
		public void click_Checkin() {
			checkin.click();
		}
		
		@FindBy(xpath = "//div[@aria-label='Wed Feb 01 2023']")
		private WebElement previousdatecheckin;
		
		public void click_previousdaycheckin() {
			previousdatecheckin.click();
		}
		
		
	//	@FindBy(xpath = "")
		
		@FindBy(xpath = "//div[@aria-label='Thu Mar 02 2023']")
		private WebElement checkoutdate;
		
		public void click_checkoutdate() {
			checkoutdate.click();
		}
		
		@FindBy(xpath = "//span[text()='Staying at']")
		private WebElement stayingat;
		
		public void click_Stayingat() {
			stayingat.click();
		}
		
		@FindBy(xpath = "//span[text()='Phuket, Thailand']")
		private WebElement thailand;
		
		public void click_thailand() {
			thailand.click();
		}

		@FindBy(xpath = "//span[text()='Bangalore']")
		private WebElement staybanglore;
		
		public void click_banglore() {
			thailand.click();
		}
		
		@FindBy(xpath = "//a[@href='/en-gb/packages?cid=1844104']")
		private WebElement goflight;
		
		public void getFlight() {
			JavascriptExecutor js=(JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", goflight);
			//goflight.click();
		}
		
		public void window()
		{
			
			String parentid =driver.getWindowHandle();
			Set<String> set = driver.getWindowHandles();
			for(String itr : set) {
				if(itr != parentid) {
					driver.switchTo().window(itr);
				}
			}
		}

}
