package org.step;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testpage.AgodaHomepage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class Agodasteps {
	
		static WebDriver driver;
	
	// static AgodaHomepage ag=null;

	
		@Given("Launch the browser")
		public void launch_the_browser() throws InterruptedException {
			Agodasteps ags=PageFactory.initElements(driver, Agodasteps.class);
			AgodaHomepage agoda= PageFactory.initElements(driver, AgodaHomepage.class);
			driver = new ChromeDriver();
		    driver.get("https://www.agoda.com/?cid=1844104");
		    driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
			Thread.sleep(10000);
			//agoda.closecoupon();
			
			
			
			
		}

		@Then("click the fligh hotel")
		public void click_the_fligh_hotel() throws InterruptedException {
			AgodaHomepage agoda= PageFactory.initElements(driver, AgodaHomepage.class);
			Thread.sleep(5000);
			agoda.clickFlight();
			
			
		}

		
		@Then("Oneway case")
		public void oneway_case() throws InterruptedException {
			AgodaHomepage agoda= PageFactory.initElements(driver, AgodaHomepage.class);
		//	agoda.clickFlight();
			//Thread.sleep(1000);
			agoda.click_roundandoneway();
			agoda.Click_oneway();
			agoda.click_economybtn();
			agoda.click_Economy();
			agoda.click_from();
			agoda.select_newDelhi();
			agoda.select_Banglore();
			agoda.click_Departuredatesafter();
			agoda.click_Adult();
			agoda.click_Contiune();
			agoda.click_Room();
			agoda.click_Checkbox();
			agoda.click_search();
			Thread.sleep(5000);
			//agoda.getFlight();
			//Thread.sleep(5000);
			
		} 
		
		@Then("quit the application")
		public void quit_the_application() {
		    driver.close();
		}
		
		
		
		@Given("Start the browser")
		public void start_the_browser() throws InterruptedException {
			Agodasteps ags=PageFactory.initElements(driver, Agodasteps.class);
			AgodaHomepage agoda= PageFactory.initElements(driver, AgodaHomepage.class);
			driver = new ChromeDriver();
		    driver.get("https://www.agoda.com/?cid=1844104");
		    driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
			Thread.sleep(10000);  
		}
		
		@Then("click Flighthotel")
		public void click_flighthotel() throws InterruptedException {
			AgodaHomepage agoda= PageFactory.initElements(driver, AgodaHomepage.class);
			Thread.sleep(5000);
			agoda.clickFlight();
		}



		@Then("roundtrip case")
		public void roundtrip_case() throws InterruptedException {
			AgodaHomepage agoda= PageFactory.initElements(driver, AgodaHomepage.class);
		//	Thread.sleep(5000);
		//	agoda.window();
		//	agoda.clickFlight();
			agoda.click_roundandoneway();
			agoda.click_Round();
			agoda.click_economybtn();
			agoda.click_Economy();
			agoda.click_from();
			agoda.select_newDelhi();
			agoda.select_Banglore();
			agoda.click_Departuredatesafter();
			agoda.click_returnnextmonth();
			agoda.click_Adult();
			agoda.click_Contiune();
			agoda.click_Room();
			agoda.click_Checkbox();
			agoda.click_search();
		}
		
		@Then("Quit the browser")
		public void quit_the_browser() {
		    driver.quit();
		}   
	}
	

